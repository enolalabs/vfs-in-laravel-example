<?php

use Illuminate\Http\UploadedFile;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamFile;

class UploadCsvTest extends TestCase
{
    public function testSuccessfulUploadingCsv()
    {
        $this->json('POST', 'upload-new-users-csv', [
            'usersCsvFile' => $this->createCsvUploadFile()
        ]);
        $this->assertResponseOk();
        $this->seeJson(["username,\"first name\",\"last name\"\n","jondoe,Jon,Doe\n","janedoe,Jane,Doe\n"]);
    }

    public function testOnlyCsvsCanBeUploadedRegardlessOfExtension()
    {
        $this->json('POST', 'upload-new-users-csv', [
            'usersCsvFile' => $this->createImageUploadFile('testFile', 'csv')
        ]);
        $this->assertResponseStatus(422);
        $this->seeJson([
            "usersCsvFile" => ["The users csv file must be a file of type: csv, txt."]
        ]);
    }

    protected function createCsvUploadFile($fileName = 'testFile')
    {
        $virtualFile = $this->createVirtualFile($fileName, 'csv')->getChild($fileName.'.csv');

        $fileResource = fopen($virtualFile->url(), 'a+');
        collect([
            ['username', 'first name', 'last name'],
            ['jondoe', 'Jon', 'Doe'],
            ['janedoe', 'Jane', 'Doe']
        ])->each(function ($fields) use ($fileResource) {
            fputcsv($fileResource, $fields);
        });
        fclose($fileResource);

        return $this->createUploadFile($virtualFile);
    }

    protected function createImageUploadFile($fileName = 'testFile', $extension = 'jpeg')
    {
        $virtualFile = $this->createVirtualFile($fileName, $extension)->getChild($fileName.'.'.$extension);
        imagejpeg(imagecreate(500, 90), $virtualFile->url());
        return $this->createUploadFile($virtualFile);
    }

    protected function createUploadFile(vfsStreamFile $file)
    {
        return new UploadedFile(
            $file->url(),
            null,
            mime_content_type($file->url()),
            null,
            null,
            true
        );
    }

    protected function createVirtualFile($filename, $extension)
    {
        return vfsStream::setup(sys_get_temp_dir(), null, [$filename.'.'.$extension => '']);
    }
}
