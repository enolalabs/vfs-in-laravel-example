<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Http\Requests\UploadCsvUsers;

Route::get('/', function () {
    return view('welcome');
});

Route::post('/upload-new-users-csv', function (UploadCsvUsers $request) {
    $uploadedFile = $request->usersCsvFile->openFile();

    $returnArray = collect();
    while (!$uploadedFile->eof()) {
        $returnArray->push($uploadedFile->fgets());
    }

    return $returnArray;
});
